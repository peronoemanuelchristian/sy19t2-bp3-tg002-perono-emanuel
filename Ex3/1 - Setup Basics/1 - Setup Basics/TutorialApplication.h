/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"
#include <OgreManualObject.h>
#include <vector>
using namespace Ogre;

//---------------------------------------------------------------------------

class TutorialApplication : public BaseApplication
{
public:
	TutorialApplication(void);
	virtual ~TutorialApplication(void);
	ManualObject* TutorialApplication::createCube(float size);
	
protected:
	virtual void createScene(void);
	bool frameStarted(const FrameEvent &evt);


private:
	SceneNode *node;
	SceneNode *nodeSun;
	SceneNode *nodeMercury;
	SceneNode *nodeVenus;
	SceneNode *nodeEarth;
	SceneNode *nodeMoon;
	SceneNode *nodeMars;

	ManualObject* createProceduralObject(float size);
	Vector3 generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2);
	SceneNode* mObject;
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------

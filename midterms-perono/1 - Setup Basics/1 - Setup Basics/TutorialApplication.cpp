/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/
#include "TutorialApplication.h"
#include "Planet.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void){
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void){
}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
		//Rotation						//Revolution
	//Mercury = 59 days				88 days			20 sec
	//Venus = 243 days				224 days		50 sec
	//Earth = 23hr					365 days		1 mintues
	//Mars = 24hr					687 days		2 minutes
	//Moon = 27 days				27 days			5 SEC
	//Sun = 28 days							------

	//v1
		//Planet *mercury = new Planet();
		//mercury->createPlanet();

	//---Planet Set values---
	//sun
	nodeSun = Planet::createPlanet(*mSceneMgr, 20, ColourValue(1,1,0.2f)); //255 255 51 
	nodeSun->setLocalRotationSpeed(10);

	//-----Child Planets----
	//mercury
	nodeMercury = Planet::createPlanet(*mSceneMgr, 3, ColourValue(0.501f, 0.501f, 0.501f)); //128 128 128
	nodeMercury->setParent(nodeSun);
		//Speed
	nodeMercury->setLocalRotationSpeed(19);
	nodeMercury->setRevolutionSpeed(26.056f); // 365/88 = 4.147 x 360 = 1492.92 to rad = 26.05636947f
	nodeMercury->getNode()->translate(70, 0, 0);

	//venus
	nodeVenus = Planet::createPlanet(*mSceneMgr, 5, ColourValue(1, 0.501f,0)); //255 128 0
	nodeVenus->setParent(nodeSun);
		//Speed
	nodeVenus->setLocalRotationSpeed(9);
	nodeVenus->setRevolutionSpeed(9.393f);		// 365/224 = 1.495 x 360 = 538.2 to rad = 9.393f
	nodeVenus->getNode()->translate(120, 0, 0);

	//mars
	nodeMars = Planet::createPlanet(*mSceneMgr, 8, ColourValue::Red);
	nodeMars->setParent(nodeSun);
		//Speed
	nodeMars->setLocalRotationSpeed(15);
	nodeMars->setRevolutionSpeed(3.336f);		// 365/687 = 0.531 x 360 = 191.16 to rad = 3.3363714f
	nodeMars->getNode()->translate(250, 0, 0);
	


	//---earth and moon-----
	//earth
	nodeEarth = Planet::createPlanet(*mSceneMgr, 10, ColourValue::Blue);
	nodeEarth->setParent(nodeSun);
		//Speed
	nodeEarth->setLocalRotationSpeed(10);
	nodeEarth->setRevolutionSpeed(6.283f);		// 365/365 = 1 x 360 = 360 to rad = 6.283f	
	nodeEarth->getNode()->translate(180, 0, 0);


	//moon
	nodeMoon = Planet::createPlanet(*mSceneMgr, 1, ColourValue::White);
	nodeMoon->setParent(nodeEarth);
	//Speed
	nodeMoon->setLocalRotationSpeed(20);
	nodeMoon->setRevolutionSpeed(300);		
	nodeMoon->getNode()->translate(210, 0, 0);

}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	/*
	//mRevolvingCube->setPosition(newX, 0, newZ);
	// or
	cube->setPosition(newx, 0, newz);
	*/
	//Sun rotation
	Degree rotation = Degree(rotationSpeed * evt.timeSinceLastFrame);
	nodeSun->getNode()->rotate(Vector3(0, 1, 0), Radian(rotation));

	//Call update framsStrated function
	nodeMercury->update(evt);
	nodeVenus->update(evt);
	nodeMars->update(evt);


	//-----
	nodeEarth->update(evt);
	nodeMoon->update(evt);

	return true;
}
ManualObject * TutorialApplication::createProceduralObject(float size)
{
	ManualObject* manual = mSceneMgr->createManualObject();

	// NOTE: The second parameter to the create method is the resource group the material will be added to.
	// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("manualMaterial", "General");
	myManualObjectMaterial->setReceiveShadows(false);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(1, 1, 1, 0);

	manual->begin("manualMaterial", RenderOperation::OT_TRIANGLE_LIST);

	// Create a circle by using rotation formula to find the points
	// And 0,0 as center

	// Store the vertices in a vector first (needed later)
	std::vector<Vector3> vertices;

	// Add the first vertex at 0,0
	vertices.push_back(Vector3::ZERO);

	// Compute for the vertices first
	int NUM_POINTS = 8;
	float anglePerPoint = 360 / NUM_POINTS;
	float radius = size / 2;
	for (int i = 1; i <= NUM_POINTS + 1; i++)
	{
		float currentAngle = anglePerPoint * i;
		// Formula: NewX = OldX x cos(theta) - OldY x sin (theta)
		float x = radius * Math::Cos(Radian(Degree(currentAngle))) - radius * Math::Sin(Radian(Degree(currentAngle)));
		// Formula: NewY = OldX x sin(theta) + OldY x cos (theta)
		float y = radius * Math::Sin(Radian(Degree(currentAngle))) + radius * Math::Cos(Radian(Degree(currentAngle)));
		Vector3 vert(x, y, 0);
		vertices.push_back(vert);
	}

	// Plot vertices
	for (int i = 0; i < NUM_POINTS + 1; i++)
	{
		manual->position(vertices[i]);
		manual->colour(ColourValue::White);
		// Compute for normals
		if (vertices[i] == Vector3::ZERO)
			manual->normal(Vector3(0, 0, 1));
		else
			Vector3 normal = generatePolygonNormal(vertices[0], vertices[i], vertices[(i + 1) % NUM_POINTS]);
	}

	// Plot indices
	for (int i = 1; i <= NUM_POINTS + 1; i++)
	{
		// For every two points, we need to make one triangle (along with center 0,0,0 at index 0)
		manual->index(0);
		manual->index(i);
		manual->index(i + 1);
	}
	// Loop back for last triangle
	manual->index(0);
	manual->index(NUM_POINTS);
	manual->index(1);

	manual->end();
	return manual;
}
Vector3 TutorialApplication::generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2)
{
	Vector3 edge1 = v1 - v0;
	Vector3 edge2 = v2 - v0;

	Vector3 normal = edge1.crossProduct(edge2);
	normal.normalise();
	return normal;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------

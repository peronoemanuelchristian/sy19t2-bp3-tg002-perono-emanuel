/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/
#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"
#include <OgreManualObject.h>
#include <vector>
using namespace Ogre;

//---------------------------------------------------------------------------
class Planet;

class TutorialApplication : public BaseApplication
{
public:
	TutorialApplication(void);
	virtual ~TutorialApplication(void);
	//No manualobject, move to planet.h
	//ManualObject* TutorialApplication::createCube(float size);
	
protected:
	virtual void createScene(void);
	bool frameStarted(const FrameEvent &evt);
	//virtual bool keyPressed(const OIS::KeyEvent &arg);
	//virtual bool TutorialApplication::keyPressed((const);
private:

	//SceneNode *cube;
	int rotationSpeed = 10;
	SceneNode* mObject;
	//SceneNode *node;

	Planet* nodeSun;
	Planet* nodeMercury;
	Planet* nodeVenus;
	Planet* nodeEarth;
	Planet* nodeMoon;
	Planet* nodeMars;

	ManualObject* createProceduralObject(float size);
	Vector3 generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2);
	//Vector3 movement;

};

//---------------------------------------------------------------------------
#endif // #ifndef __TutorialApplication_h_
//---------------------------------------------------------------------------

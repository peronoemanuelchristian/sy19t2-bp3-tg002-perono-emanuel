#include "TutorialApplication.h"
#include "Planet.h"
using namespace Ogre;

Planet::Planet(void) {
}
Planet::~Planet(void) {
}

Planet::Planet(SceneNode* node){

	mNode = node;
}
Planet* Planet::createPlanet(SceneManager &sceneManager, float size, ColourValue colour){
	
	//ManualObject* triangle = createCube(20);
	////mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);
	////Alternative
	//cube = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	//cube->attachObject(triangle);

	//ManualObject *planet = mSceneMgr->createManualObject();

	ManualObject *planet = sceneManager.createManualObject();
	planet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	planet->colour(colour);
	planet->position(size, -size, size);//0
	planet->position(size, size, size);//1
	planet->position(-size, size, size);//2
	planet->position(-size, -size, size);//3
	
	//Back
	planet->position(size, -size, -size);
	planet->position(size, size, -size);
	planet->position(-size, size, -size);
	planet->position(-size, -size, -size);

	//---front
	planet->index(0);
	planet->index(2);
	planet->index(3);

	planet->index(0);
	planet->index(1);
	planet->index(2);
	//----back
	planet->index(4);
	planet->index(7);
	planet->index(6);

	planet->index(6);
	planet->index(5);
	planet->index(4);
	//----top
	planet->index(6);
	planet->index(2);
	planet->index(1);

	planet->index(1);
	planet->index(5);
	planet->index(6);
	//----bot
	planet->index(7);
	planet->index(0);
	planet->index(3);

	planet->index(7);
	planet->index(4);
	planet->index(0);
	//----left
	planet->index(2);
	planet->index(6);
	planet->index(3);

	planet->index(6);
	planet->index(7);
	planet->index(3);
	//----right
	planet->index(1);
	planet->index(4);
	planet->index(5);

	planet->index(1);
	planet->index(0);
	planet->index(4);

	planet->end();

	//----TutorialApplication::createScene(void)------
	SceneNode *node = sceneManager.getRootSceneNode()->createChildSceneNode();
	node->attachObject(planet);
	//creates dynamic memory instances of Planet binded (node)
	return new Planet(node);


	//delete planet; //Memory leak
}

void Planet::update(const FrameEvent& evt)
{
	Degree rotation = Degree(mLocalRotationSpeed * evt.timeSinceLastFrame);
	mNode->rotate(Vector3(0, 1, 0), Radian(rotation));
	/*
	Earth is at (6,7)
	Moon is at (9,10)

	Moon position relative to earth: (3,3). Rotate 30 degrees

	newX = oldX * cos(angle) + oldZ * sin(angle)
	newZ = oldX * -sin(angle) + oldZ * cos(angle)

	newX = 3 * cos(30) + 3 * sin(30)
	newZ = 3 * -sin(30) + 3 * cos(30)

	newX =  2.6 + 1.5
	newX = 4.1
	newZ = -1.5 + 2.6
	newZ = 1.1
	
	New Position of moon relative to earth: (4.1 , 1.1)
	New position of moon relative to sun: (10.1, 8.1)

	*/
	Degree revolution = Degree(mRevolutionSpeed * evt.timeSinceLastFrame);
	//(X,Y,Z) position
	float oldx = mNode->getPosition().x - mParent->mNode->getPosition().x;
	float oldz = mNode->getPosition().z - mParent->mNode->getPosition().z;
	float oldy = mNode->getPosition().y;
	
	float newx = (oldx
		*Math::Cos(Radian(revolution)) + oldz
		*Math::Sin(Radian(revolution)) )+ mParent->getNode()->getPosition().x;

	float newz = (oldx 
		*-Math::Sin(Radian(revolution)) + oldz
		*Math::Cos(Radian(revolution)) )+  mParent->getNode()->getPosition().z;

	/*
	Degree revolutionDegrees = Degree(45.0f * evt.timeSinceLastFrame);

	//float oldX = mRevolvingCube->getPosition().x;
	//float oldZ = mRevolvingCube->getPosition().z;

	float newx = cube->getPosition().x*Math::Cos(Radian(revolutionDegrees)) + cube->getPosition().z *Math::Sin(Radian(revolutionDegrees));
	float newz = cube->getPosition().x*-Math::Sin(Radian(revolutionDegrees)) + cube->getPosition().z *Math::Cos(Radian(revolutionDegrees));

	cube->setPosition(newx, 0, newz);

	*/

	mNode->setPosition(newx, oldy, newz);

}

//get node
SceneNode* Planet::getNode(){

	return mNode;
}

//Set Planet Parent to mParent
void Planet::setParent(Planet* parent){

	mParent = parent;
}

//get parent planet to return mParent
Planet* Planet::getParent(){

	return mParent;
}

//planet rotation
void Planet::setLocalRotationSpeed(float speed){

	mLocalRotationSpeed = speed;
}

//revolution speed
void Planet::setRevolutionSpeed(float speed){

	//Using 365 and 182.5.How can you get 2 ?
	//	365 / 182.5 = 2
	//	If earth was revolving at 360 degrees per minute.
	//	A planet that has a rev speed for 182.5 days revolves around the sun at 720	 degrees per minute

	mRevolutionSpeed = speed;
}

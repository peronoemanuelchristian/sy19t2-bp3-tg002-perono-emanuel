#include "TutorialApplication.h"
#include "Planet.h"
using namespace Ogre;

Planet::Planet(void) {
}
Planet::~Planet(void) {
}

Planet::Planet(SceneNode* node){

	mNode = node;
}
Planet* Planet::createPlanet(SceneManager &sceneManager, float size, const String& materialName, ColourValue colour){
	
	//ManualObject* triangle = createCube(20);
	////mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);
	////Alternative
	//cube = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	//cube->attachObject(triangle);

	//ManualObject *planet = mSceneMgr->createManualObject();

	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("materialName", "General");
	
	// Modify some properties of materials
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(0, 0, 1, 0); //color
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(0, 0, 0.3, 0);

	// Replace �BaseWhite� with the new material
	//manual->begin("manualMaterial", RenderOperation::OT_TRIANGLE_LIST);

	
	//---------------------ANSWER 6-1----------------
	/*Modify your values to answer the following questions :

	What happens if you have a yellow light source, and a blue diffuse color ?
	=The colored turned blushish color.

	What happens if you have a yellow light source and a green diffuse color ?
	= It is still lightish green. 

	*/


	ManualObject *planet = sceneManager.createManualObject();
	planet->begin(materialName, RenderOperation::OT_TRIANGLE_LIST);

	Vector3 v0, v1, v2; //assume they have values
	Vector3 edge1 = v1 - v0;
	Vector3 edge2 = v2 - v0;
	Vector3 normal = edge1.crossProduct(edge2);
	normal.normalise();

	//front
	planet->colour(colour);
	planet->position(size, -size, size); planet->normal(Vector3(0, 0, 1));//0
	planet->position(size, size, size); planet->normal(Vector3(0, 0, 1));//1
	planet->position(-size, size, size);planet->normal(Vector3(0, 0, 1));//2
	planet->position(-size, -size, size);planet->normal(Vector3(0, 0, 1));//3
	
	//Back
	planet->position(size, -size, -size); 	planet->normal(Vector3(0, 0, -1)); //4
	planet->position(size, size, -size); 	planet->normal(Vector3(0, 0, -1));//5
	planet->position(-size, size, -size);	planet->normal(Vector3(0, 0, -1));//6
	planet->position(-size, -size, -size); planet->normal(Vector3(0, 0, -1));//7

	//Top
	planet->position(-size, size, size); 	planet->normal(Vector3(0, 1, 0));//8
	planet->position(size, size, size); 	planet->normal(Vector3(0, 1, 0));//9
	planet->position(size, size, -size);	planet->normal(Vector3(0, 1, 0));//10
	planet->position(-size, size, -size); planet->normal(Vector3(0, 1, 0));//11

	//bottom
	planet->position(-size, -size, -size); 	planet->normal(Vector3(0, -1, 0));//12
	planet->position(size, -size, -size); 	planet->normal(Vector3(0, -1, 0));//13
	planet->position(size, -size, size);	planet->normal(Vector3(0, -1, 0));//14
	planet->position(-size, -size, size); planet->normal(Vector3(0, -1, 0));//15

	//left
	planet->position(-size, -size, -size); 	planet->normal(Vector3(-1, 0, 0));//16
	planet->position(-size, -size, size); 	planet->normal(Vector3(-1, 0, 0));//17
	planet->position(-size, size, size);	planet->normal(Vector3(-1, 0, 0));//18
	planet->position(-size, size, -size); planet->normal(Vector3(-1, 0, 0));//19

	//right
	planet->position(size, -size, size); 	planet->normal(Vector3(1, 0, 0));//20
	planet->position(size, -size, -size); 	planet->normal(Vector3(1, 0, 0));//21
	planet->position(size, size, -size);	planet->normal(Vector3(1, 0, 0));//22
	planet->position(size, size, size); planet->normal(Vector3(1, 0, 0));//23

	//---front
	//planet->normal(Vector3(0, 0, 1));
	planet->index(0);
	planet->index(2);
	planet->index(3);

	planet->index(0);
	planet->index(1);
	planet->index(2);
	//----back
	planet->index(4);
	planet->index(6);
	planet->index(5);

	planet->index(4);
	planet->index(7);
	planet->index(6);


	//----top
	planet->index(8);
	planet->index(10);
	planet->index(11);

	planet->index(8);
	planet->index(9);
	planet->index(10);

	//----bot
	planet->index(12);
	planet->index(14);
	planet->index(15);

	planet->index(12);
	planet->index(13);
	planet->index(14);

	//----left
	planet->index(16);
	planet->index(18);
	planet->index(19);

	planet->index(16);
	planet->index(17);
	planet->index(18);

	//----right
	planet->index(20);
	planet->index(22);
	planet->index(23);

	planet->index(20);
	planet->index(21);
	planet->index(22);

	planet->end();

	//----TutorialApplication::createScene(void)------
	SceneNode *node = sceneManager.getRootSceneNode()->createChildSceneNode();
	node->attachObject(planet);

	Light *light = sceneManager.createLight();
	light->setType(Light::LightTypes::LT_POINT);
	//Light source
	light->setDiffuseColour(ColourValue(1, 1, 0)); //yellow (1,1,0) - white(1,1,1)
	light->setAttenuation(325, 0.0f, 0.014, 0.0007);
	light->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	light->setPosition(Vector3(0, 0, 0));


	//ambient light
	sceneManager.setAmbientLight(ColourValue(0.3f, 0.3f, 0.3f));

	//creates dynamic memory instances of Planet binded (node)
	return new Planet(node);
}

void Planet::update(const FrameEvent& evt)
{
	Degree rotation = Degree(mLocalRotationSpeed * evt.timeSinceLastFrame);
	mNode->rotate(Vector3(0, 1, 0), Radian(rotation));
	/*
	Earth is at (6,7)
	Moon is at (9,10)

	Moon position relative to earth: (3,3). Rotate 30 degrees

	newX = oldX * cos(angle) + oldZ * sin(angle)
	newZ = oldX * -sin(angle) + oldZ * cos(angle)

	newX = 3 * cos(30) + 3 * sin(30)
	newZ = 3 * -sin(30) + 3 * cos(30)

	newX =  2.6 + 1.5
	newX = 4.1
	newZ = -1.5 + 2.6
	newZ = 1.1
	
	New Position of moon relative to earth: (4.1 , 1.1)
	New position of moon relative to sun: (10.1, 8.1)

	*/
	//Degree revolution = Degree(mRevolutionSpeed * evt.timeSinceLastFrame);
	////(X,Y,Z) position
	//float oldx = mNode->getPosition().x - mParent->mNode->getPosition().x;
	//float oldz = mNode->getPosition().z - mParent->mNode->getPosition().z;
	//float oldy = mNode->getPosition().y;
	//
	//float newx = (oldx
	//	*Math::Cos(Radian(revolution)) + oldz
	//	*Math::Sin(Radian(revolution)) )+ mParent->getNode()->getPosition().x;

	//float newz = (oldx 
	//	*-Math::Sin(Radian(revolution)) + oldz
	//	*Math::Cos(Radian(revolution)) )+  mParent->getNode()->getPosition().z;

	/*
	Degree revolutionDegrees = Degree(45.0f * evt.timeSinceLastFrame);

	//float oldX = mRevolvingCube->getPosition().x;
	//float oldZ = mRevolvingCube->getPosition().z;

	float newx = cube->getPosition().x*Math::Cos(Radian(revolutionDegrees)) + cube->getPosition().z *Math::Sin(Radian(revolutionDegrees));
	float newz = cube->getPosition().x*-Math::Sin(Radian(revolutionDegrees)) + cube->getPosition().z *Math::Cos(Radian(revolutionDegrees));

	cube->setPosition(newx, 0, newz);

	*/

	//mNode->setPosition(newx, oldy, newz);

}

//get node
SceneNode* Planet::getNode(){

	return mNode;
}

//Set Planet Parent to mParent
void Planet::setParent(Planet* parent){

	mParent = parent;
}

//get parent planet to return mParent
Planet* Planet::getParent(){

	return mParent;
}

//planet rotation
void Planet::setLocalRotationSpeed(float speed){

	mLocalRotationSpeed = speed;
}

//revolution speed
void Planet::setRevolutionSpeed(float speed){

	//Using 365 and 182.5.How can you get 2 ?
	//	365 / 182.5 = 2
	//	If earth was revolving at 360 degrees per minute.
	//	A planet that has a rev speed for 182.5 days revolves around the sun at 720	 degrees per minute

	mRevolutionSpeed = speed;
}

#ifndef __Planet_h_
#define ____Planet_h__h_

#include "BaseApplication.h"
#include <OgreManualObject.h>
#include <vector>
using namespace Ogre;

//Translation & Rotation computations should all be inside Planet class
class Planet{
public:
	Planet();
	~Planet();

	//As a tip, here are two of the functions used in the answer key.
	Planet(SceneNode* node);
	//v2 
	static Planet* createPlanet(SceneManager &sceneManager, float size, const String& materialName, ColourValue colour);

	//constantly update the child planets position 
	void update(const FrameEvent& evt);
	//void revolution(const FrameEvent& evt); //with moon revolution

	//Getter:
	SceneNode * getNode();
	void setParent(Planet *parent);
	Planet *getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);

	/*TutorialApplication(void);
	virtual ~TutorialApplication(void);
	ManualObject* TutorialApplication::createCube(float size);*/

protected:
	//virtual void createScene(void);
	////bool frameStarted(const FrameEvent &evt);
	////virtual bool keyPressed(const OIS::KeyEvent &arg);
	////virtual bool TutorialApplication::keyPressed((const);

private:
	//SceneNode *cube;
	//int mAcceleration = 1;

	//position
	SceneNode* mNode;

	Planet* mParent;
	//Degree localRotation;
	//Degree revolutionDegrees;

	//nonstatic member
	//SceneNode *node;

	float mLocalRotationSpeed;
	float mRevolutionSpeed;

	//ManualObject* createProceduralObject(float size);
	Vector3 generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2);
	SceneNode* mObject;
	Vector3 movement;
};

//---------------------------------------------------------------------------

#endif // #ifndef __Planet_h_

//---------------------------------------------------------------------------
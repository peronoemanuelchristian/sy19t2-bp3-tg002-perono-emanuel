/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}


ManualObject* TutorialApplication::createCube(float size)
{
	ManualObject *triangle = mSceneMgr->createManualObject();
	triangle->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	/*
	triangle->position(0, 10, 0);
	triangle->position(-10, 0, 0);
	triangle->position(10, 0, 0);
	---------
	Front
	triangle->position(size, -size, size);    //	0	
	triangle->position(-size, size, size);    //	1
	triangle->position(-size, -size, size);    //	2
	triangle->position(-size, -size, size);    //	3
	Back
	triangle->position(size, -size, size);    //	4	
	triangle->position(-size, size, size);    //	5
	triangle->position(-size, -size, size);    //	6
	triangle->position(-size, -size, size);    //	7
	manual->index(0);
	manual->index(1);
	manual->index(2);
	manual->index(3);
	manual->index(4);
	manual->index(5);
	manual->index(6);
	manual->index(7);
	*/
	//size = 10;
	//Front
	triangle->position(size, -size, size);
	triangle->position(size, size, size);
	triangle->position(-size, size, size);
	triangle->position(-size, -size, size);
	//Back
	triangle->position(size, -size, -size);
	triangle->position(size, size, -size);
	triangle->position(-size, size, -size);
	triangle->position(-size, -size, -size);
	//---front
	triangle->index(0);
	triangle->index(2);
	triangle->index(3);

	triangle->index(0);
	triangle->index(1);
	triangle->index(2);
	//----back
	triangle->index(4);
	triangle->index(7);
	triangle->index(6);

	triangle->index(6);
	triangle->index(5);
	triangle->index(4);
	//----top
	triangle->index(6);
	triangle->index(2);
	triangle->index(1);

	triangle->index(1);
	triangle->index(5);
	triangle->index(6);
	//----bot
	triangle->index(7);
	triangle->index(0);
	triangle->index(3);

	triangle->index(7);
	triangle->index(4);
	triangle->index(0);
	//----left
	triangle->index(2);
	triangle->index(6);
	triangle->index(3);

	triangle->index(6);
	triangle->index(7);
	triangle->index(3);
	//----right
	triangle->index(1);
	triangle->index(4);
	triangle->index(5);

	triangle->index(1);
	triangle->index(0);
	triangle->index(4);


	triangle->end();
	return triangle;
}



//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	ManualObject* triangle = createCube(20);
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	return true;
}
ManualObject * TutorialApplication::createProceduralObject(float size)
{
	ManualObject* manual = mSceneMgr->createManualObject();

	// NOTE: The second parameter to the create method is the resource group the material will be added to.
	// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("manualMaterial", "General");
	myManualObjectMaterial->setReceiveShadows(false);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(1, 1, 1, 0);

	manual->begin("manualMaterial", RenderOperation::OT_TRIANGLE_LIST);

	// Create a circle by using rotation formula to find the points
	// And 0,0 as center

	// Store the vertices in a vector first (needed later)
	std::vector<Vector3> vertices;

	// Add the first vertex at 0,0
	vertices.push_back(Vector3::ZERO);

	// Compute for the vertices first
	int NUM_POINTS = 8;
	float anglePerPoint = 360 / NUM_POINTS;
	float radius = size / 2;
	for (int i = 1; i <= NUM_POINTS + 1; i++)
	{
		float currentAngle = anglePerPoint * i;
		// Formula: NewX = OldX x cos(theta) - OldY x sin (theta)
		float x = radius * Math::Cos(Radian(Degree(currentAngle))) - radius * Math::Sin(Radian(Degree(currentAngle)));
		// Formula: NewY = OldX x sin(theta) + OldY x cos (theta)
		float y = radius * Math::Sin(Radian(Degree(currentAngle))) + radius * Math::Cos(Radian(Degree(currentAngle)));
		Vector3 vert(x, y, 0);
		vertices.push_back(vert);
	}

	// Plot vertices
	for (int i = 0; i < NUM_POINTS + 1; i++)
	{
		manual->position(vertices[i]);
		manual->colour(ColourValue::White);
		// Compute for normals
		if (vertices[i] == Vector3::ZERO)
			manual->normal(Vector3(0, 0, 1));
		else
			Vector3 normal = generatePolygonNormal(vertices[0], vertices[i], vertices[(i + 1) % NUM_POINTS]);
	}

	// Plot indices
	for (int i = 1; i <= NUM_POINTS + 1; i++)
	{
		// For every two points, we need to make one triangle (along with center 0,0,0 at index 0)
		manual->index(0);
		manual->index(i);
		manual->index(i + 1);
	}
	// Loop back for last triangle
	manual->index(0);
	manual->index(NUM_POINTS);
	manual->index(1);

	manual->end();
	return manual;
}
Vector3 TutorialApplication::generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2)
{
	Vector3 edge1 = v1 - v0;
	Vector3 edge2 = v2 - v0;

	Vector3 normal = edge1.crossProduct(edge2);
	normal.normalise();
	return normal;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
